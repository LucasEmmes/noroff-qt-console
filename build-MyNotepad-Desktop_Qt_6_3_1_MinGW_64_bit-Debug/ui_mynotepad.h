/********************************************************************************
** Form generated from reading UI file 'mynotepad.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYNOTEPAD_H
#define UI_MYNOTEPAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyNotepad
{
public:
    QAction *action_Open;
    QAction *action_Save;
    QAction *actionSave_As;
    QAction *actionE_xit;
    QAction *actionCu_t;
    QAction *action_Copy;
    QAction *action_Paste;
    QAction *action_About;
    QAction *actionAbout_Qt;
    QAction *action_New;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *plainTextEdit;
    QMenuBar *menubar;
    QMenu *menu_File;
    QMenu *menu_Edit;
    QMenu *menu_Help;
    QStatusBar *statusbar;
    QToolBar *toolBar_file;
    QToolBar *toolBar_edit;

    void setupUi(QMainWindow *MyNotepad)
    {
        if (MyNotepad->objectName().isEmpty())
            MyNotepad->setObjectName(QString::fromUtf8("MyNotepad"));
        MyNotepad->resize(800, 600);
        action_Open = new QAction(MyNotepad);
        action_Open->setObjectName(QString::fromUtf8("action_Open"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icons/document-open"), QSize(), QIcon::Normal, QIcon::Off);
        action_Open->setIcon(icon);
        action_Save = new QAction(MyNotepad);
        action_Save->setObjectName(QString::fromUtf8("action_Save"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/icons/document-save"), QSize(), QIcon::Normal, QIcon::Off);
        action_Save->setIcon(icon1);
        actionSave_As = new QAction(MyNotepad);
        actionSave_As->setObjectName(QString::fromUtf8("actionSave_As"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/icons/document-save-as"), QSize(), QIcon::Normal, QIcon::Off);
        actionSave_As->setIcon(icon2);
        actionE_xit = new QAction(MyNotepad);
        actionE_xit->setObjectName(QString::fromUtf8("actionE_xit"));
        actionCu_t = new QAction(MyNotepad);
        actionCu_t->setObjectName(QString::fromUtf8("actionCu_t"));
        actionCu_t->setEnabled(false);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/icons/edit-cut"), QSize(), QIcon::Normal, QIcon::Off);
        actionCu_t->setIcon(icon3);
        action_Copy = new QAction(MyNotepad);
        action_Copy->setObjectName(QString::fromUtf8("action_Copy"));
        action_Copy->setEnabled(false);
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/icons/edit-copy"), QSize(), QIcon::Normal, QIcon::Off);
        action_Copy->setIcon(icon4);
        action_Paste = new QAction(MyNotepad);
        action_Paste->setObjectName(QString::fromUtf8("action_Paste"));
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/icons/edit-paste"), QSize(), QIcon::Normal, QIcon::Off);
        action_Paste->setIcon(icon5);
        action_About = new QAction(MyNotepad);
        action_About->setObjectName(QString::fromUtf8("action_About"));
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/icons/noroff"), QSize(), QIcon::Normal, QIcon::Off);
        action_About->setIcon(icon6);
        actionAbout_Qt = new QAction(MyNotepad);
        actionAbout_Qt->setObjectName(QString::fromUtf8("actionAbout_Qt"));
        action_New = new QAction(MyNotepad);
        action_New->setObjectName(QString::fromUtf8("action_New"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icons/document-new"), QSize(), QIcon::Normal, QIcon::Off);
        action_New->setIcon(icon7);
        centralwidget = new QWidget(MyNotepad);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        plainTextEdit = new QPlainTextEdit(centralwidget);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        verticalLayout->addWidget(plainTextEdit);

        MyNotepad->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MyNotepad);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 26));
        menu_File = new QMenu(menubar);
        menu_File->setObjectName(QString::fromUtf8("menu_File"));
        menu_Edit = new QMenu(menubar);
        menu_Edit->setObjectName(QString::fromUtf8("menu_Edit"));
        menu_Help = new QMenu(menubar);
        menu_Help->setObjectName(QString::fromUtf8("menu_Help"));
        MyNotepad->setMenuBar(menubar);
        statusbar = new QStatusBar(MyNotepad);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MyNotepad->setStatusBar(statusbar);
        toolBar_file = new QToolBar(MyNotepad);
        toolBar_file->setObjectName(QString::fromUtf8("toolBar_file"));
        MyNotepad->addToolBar(Qt::TopToolBarArea, toolBar_file);
        toolBar_edit = new QToolBar(MyNotepad);
        toolBar_edit->setObjectName(QString::fromUtf8("toolBar_edit"));
        MyNotepad->addToolBar(Qt::TopToolBarArea, toolBar_edit);

        menubar->addAction(menu_File->menuAction());
        menubar->addAction(menu_Edit->menuAction());
        menubar->addAction(menu_Help->menuAction());
        menu_File->addAction(action_New);
        menu_File->addAction(action_Open);
        menu_File->addAction(action_Save);
        menu_File->addAction(actionSave_As);
        menu_File->addSeparator();
        menu_File->addAction(actionE_xit);
        menu_Edit->addAction(actionCu_t);
        menu_Edit->addAction(action_Copy);
        menu_Edit->addAction(action_Paste);
        menu_Help->addAction(action_About);
        menu_Help->addAction(actionAbout_Qt);
        toolBar_file->addAction(action_New);
        toolBar_file->addAction(action_Open);
        toolBar_file->addAction(action_Save);
        toolBar_file->addAction(actionSave_As);
        toolBar_edit->addAction(actionCu_t);
        toolBar_edit->addAction(action_Copy);
        toolBar_edit->addAction(action_Paste);

        retranslateUi(MyNotepad);
        QObject::connect(actionE_xit, &QAction::triggered, MyNotepad, qOverload<>(&QMainWindow::close));
        QObject::connect(actionCu_t, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::cut));
        QObject::connect(action_Copy, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::copy));
        QObject::connect(action_Paste, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::paste));

        QMetaObject::connectSlotsByName(MyNotepad);
    } // setupUi

    void retranslateUi(QMainWindow *MyNotepad)
    {
        MyNotepad->setWindowTitle(QString());
        action_Open->setText(QCoreApplication::translate("MyNotepad", "&Open", nullptr));
#if QT_CONFIG(statustip)
        action_Open->setStatusTip(QCoreApplication::translate("MyNotepad", "Open an existing file", nullptr));
#endif // QT_CONFIG(statustip)
        action_Save->setText(QCoreApplication::translate("MyNotepad", "&Save", nullptr));
#if QT_CONFIG(statustip)
        action_Save->setStatusTip(QCoreApplication::translate("MyNotepad", "Save document to disk", nullptr));
#endif // QT_CONFIG(statustip)
        actionSave_As->setText(QCoreApplication::translate("MyNotepad", "Save &As", nullptr));
#if QT_CONFIG(statustip)
        actionSave_As->setStatusTip(QCoreApplication::translate("MyNotepad", "Save document to under a new name", nullptr));
#endif // QT_CONFIG(statustip)
        actionE_xit->setText(QCoreApplication::translate("MyNotepad", "E&xit", nullptr));
#if QT_CONFIG(statustip)
        actionE_xit->setStatusTip(QCoreApplication::translate("MyNotepad", "Exit the application", nullptr));
#endif // QT_CONFIG(statustip)
        actionCu_t->setText(QCoreApplication::translate("MyNotepad", "Cu&t", nullptr));
#if QT_CONFIG(statustip)
        actionCu_t->setStatusTip(QCoreApplication::translate("MyNotepad", "Cut the current selection to clipboard", nullptr));
#endif // QT_CONFIG(statustip)
        action_Copy->setText(QCoreApplication::translate("MyNotepad", "&Copy", nullptr));
#if QT_CONFIG(statustip)
        action_Copy->setStatusTip(QCoreApplication::translate("MyNotepad", "Copy Current selection to clipboard", nullptr));
#endif // QT_CONFIG(statustip)
        action_Paste->setText(QCoreApplication::translate("MyNotepad", "&Paste", nullptr));
#if QT_CONFIG(statustip)
        action_Paste->setStatusTip(QCoreApplication::translate("MyNotepad", "Paste current clipboard content", nullptr));
#endif // QT_CONFIG(statustip)
        action_About->setText(QCoreApplication::translate("MyNotepad", "&About", nullptr));
#if QT_CONFIG(statustip)
        action_About->setStatusTip(QCoreApplication::translate("MyNotepad", "Show the applicatio'ns About box", nullptr));
#endif // QT_CONFIG(statustip)
        actionAbout_Qt->setText(QCoreApplication::translate("MyNotepad", "About &Qt", nullptr));
#if QT_CONFIG(statustip)
        actionAbout_Qt->setStatusTip(QCoreApplication::translate("MyNotepad", "Show the Qt library's About box", nullptr));
#endif // QT_CONFIG(statustip)
        action_New->setText(QCoreApplication::translate("MyNotepad", "&New", nullptr));
#if QT_CONFIG(statustip)
        action_New->setStatusTip(QCoreApplication::translate("MyNotepad", "Create new file", nullptr));
#endif // QT_CONFIG(statustip)
        menu_File->setTitle(QCoreApplication::translate("MyNotepad", "&File", nullptr));
        menu_Edit->setTitle(QCoreApplication::translate("MyNotepad", "&Edit", nullptr));
        menu_Help->setTitle(QCoreApplication::translate("MyNotepad", "&Help", nullptr));
        toolBar_file->setWindowTitle(QCoreApplication::translate("MyNotepad", "toolBar", nullptr));
        toolBar_edit->setWindowTitle(QCoreApplication::translate("MyNotepad", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MyNotepad: public Ui_MyNotepad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYNOTEPAD_H
