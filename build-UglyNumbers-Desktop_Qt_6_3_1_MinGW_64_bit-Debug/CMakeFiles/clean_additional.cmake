# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles\\UglyNumbers_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\UglyNumbers_autogen.dir\\ParseCache.txt"
  "UglyNumbers_autogen"
  )
endif()
