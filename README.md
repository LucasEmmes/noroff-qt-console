# QT

## Background  
The purpose of these projects is to get more familiar with Qt creator and Qt app design.  

## How to run:  
To run, you need [Qt Creator](https://www.qt.io/download)  
Open the project.
Then build and run the app by pressing on the top green arrow.