#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include "calculate.cpp"

int main(int argc, char const *argv[])
{
    QTextStream qout(stdout);
    QTextStream qin(stdin);

    float result;
    try
    {
        std::vector<std::string> tokens = get_input();
        result = parse_tokens(tokens);
    }
    catch(const std::string error)
    {
        qout << "Not good":
        qout.flush();
        return 1;
    }

    // Print out result :)
    qout << "The result is: " << result << "\n";
    qout.flush();
    return 0;
}
